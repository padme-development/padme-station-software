const status = {
  IDLE: "idle",
  SUCCESS: "success",
  FAILED: "failed",
};

export { status };
