// Information of database (MongoDB)
const {
  MONGO_USER,
  MONGO_PASSWORD,
  MONGO_HOST,
  MONGO_PORT,
  MONGO_DB,
  MONITORING_API_ADDRESS,
  STATION_ID,
} = process.env;

module.exports = {
  mongoURI: `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}` /* mongodb://username:password@host:port/database */,
  monitoringApiUrl: MONITORING_API_ADDRESS,
  stationId: STATION_ID,
};
