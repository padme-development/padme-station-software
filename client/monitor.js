const axios = require('axios');

const config = require('../config/keys');
const { instance: dockerInstance } = require('../federated/flDocker');
const { getKeycloakAccessToken } = require('../utils/utility');

// ! These strings should match the JobState in PHT monitoring backend (app > utils > JobState)
const JobState = {
  IDLE: 'idle',
  CANCELLED: 'cancelled',
  RUNNING: 'running',
  TRANSMISSION: 'transmission',
  FINISHED: 'finished',
  FAILED: 'failed',
  WAITING: 'waiting',
};

const exitCodeToState = (exitCode) => {
  switch (exitCode) {
    case 0:
      return JobState.FINISHED;
    case 1:
    case 139:
    case 255:
      return JobState.FAILED;
    case 137:
    case 143:
      return JobState.CANCELLED;
    default:
      return JobState.IDLE;
  }
};

class ContainerMetricsMonitor {
  constructor(metricsEndpoint) {
    // Docker connection configuration
    this.dockerClient = dockerInstance;
    this.metricsEndpoint = metricsEndpoint;
    this.activeMonitors = new Map();
    this.token = null;
    this.tokenExpiration = null;
  }

  async startMetricsCollection(container) {
    // Prevent multiple monitors for the same container
    if (this.activeMonitors.has(container)) {
      console.warn(
        `Metrics monitor already running for container ${container}`
      );
      return;
    }

    try {
      // Verify container exists and is running
      const containerObj = this.dockerClient.getContainer(container);
      const containerInfo = await containerObj.inspect();

      if (!containerInfo.State.Running) {
        throw new Error('Container is not in running state');
      }

      // Notify 'running' state of job to monitoring api
      const jobId = containerInfo.Name.replace('/', '');
      await this.updateJobState(jobId, JobState.RUNNING);

      // Create a metrics collection interval
      const intervalId = setInterval(async () => {
        try {
          // Recheck container status
          const currentInfo = await containerObj.inspect();

          // If not in running state, notify current state to monitoring api
          if (!currentInfo.State.Running) {
            const currentJobState = exitCodeToState(currentInfo.State.ExitCode);
            await this.updateJobState(jobId, currentJobState);

            this.stopMetricsCollection(container);
            return;
          }

          // Collect and send metrics
          const metrics = await this.collectContainerMetrics(containerObj);
          await this.sendMetricsToEndpoint(jobId, metrics);
        } catch (error) {
          console.error(
            `Metrics collection error for container ${container}:`,
            error
          );
          this.stopMetricsCollection(container);
        }
      }, 10000); // Collect metrics every 10 seconds

      // Store the interval for later cancellation
      this.activeMonitors.set(container, intervalId);

      console.log(`Started metrics monitoring for container ${container}`);
    } catch (error) {
      console.error(
        `Failed to start metrics monitoring for container ${container}:`,
        error
      );
      throw error;
    }
  }

  async collectContainerMetrics(containerObj) {
    // Collect container statistics
    const [statsStream, containerInfo] = await Promise.all([
      containerObj.stats({ stream: false }),
      containerObj.inspect(),
    ]);

    // CPU stats extraction and usage calcuation
    const cpuStats = statsStream.cpu_stats || {};
    const prevCpuStats = statsStream.precpu_stats || {};
    const cpuUsage = this.calculateCPUPercentage(cpuStats, prevCpuStats);

    // Memory metrics
    const memoryStats = statsStream.memory_stats || {};
    const memoryUsage = this.calculateMemoryUsage(memoryStats);

    // Network metrics
    const networkStats = this.extractNetworkStats(statsStream);

    return {
      containerId: containerObj.id,
      containerName: containerInfo.Name,
      timestamp: new Date().toISOString(),
      cpu: {
        usage: cpuUsage.toFixed(2),
        cores: cpuStats.cpu_usage?.percpu_usage?.length || 0,
      },
      memory: memoryUsage,
      network: networkStats,
    };
  }

  calculateCPUPercentage(cpuStats, prevCpuStats) {
    try {
      // Ensure we have the necessary CPU usage statistics
      const currentCpuTotal = cpuStats.cpu_usage?.total_usage || 0;
      const prevCpuTotal = prevCpuStats.cpu_usage?.total_usage || 0;
      const currentSystemCpuTotal = cpuStats.system_cpu_usage || 0;
      const prevSystemCpuTotal = prevCpuStats.system_cpu_usage || 0;

      // Calculate deltas
      const cpuDelta = currentCpuTotal - prevCpuTotal;
      const systemDelta = currentSystemCpuTotal - prevSystemCpuTotal;

      // Determine number of CPUs
      const numberOfCPUs = cpuStats.cpu_usage?.percpu_usage?.length || 1;

      // Prevent division by zero and handle edge cases
      if (systemDelta <= 0 || cpuDelta < 0) {
        return 0;
      }

      // Calculate CPU percentage
      const cpuPercentage = (cpuDelta / systemDelta) * numberOfCPUs * 100.0;

      // Ensure we don't return negative or extremely large values
      return Math.max(0, Math.min(cpuPercentage, 100));
    } catch (error) {
      console.error('CPU percentage calculation error:', error);
      return 0;
    }
  }

  calculateMemoryUsage(memoryStats) {
    try {
      // Safely extract memory usage
      const usage = memoryStats.usage || 0;
      const limit = memoryStats.limit || 1; // Prevent division by zero

      // Calculate percentage with safeguards
      const percentage =
        usage > 0
          ? Math.min((usage / limit) * 100, 100) // Cap at 100%
          : 0;

      return {
        used: usage,
        limit: limit,
        percentage: percentage.toFixed(2),
      };
    } catch (error) {
      console.error('Memory usage calculation error:', error);
      return {
        used: 0,
        limit: 0,
        percentage: 0,
      };
    }
  }

  extractNetworkStats(statsStream) {
    try {
      // Safely extract network statistics
      const networks = statsStream.networks || {};

      // If no networks, return zero values
      if (Object.keys(networks).length === 0) {
        return {
          rxBytes: 0,
          txBytes: 0,
        };
      }

      // Take the first network interface's stats
      const networkInterface = Object.values(networks)[0];

      return {
        rxBytes: networkInterface.rx_bytes || 0,
        txBytes: networkInterface.tx_bytes || 0,
      };
    } catch (error) {
      console.error('Network stats extraction error:', error);
      return {
        rxBytes: 0,
        txBytes: 0,
      };
    }
  }

  async getToken() {
    if (!this.token || new Date() >= this.tokenExpiration) {
      const tokenResponse = await getKeycloakAccessToken();
      this.token = tokenResponse.access_token;
      const expiresIn = tokenResponse.expires_in || 60 * 60; // Default to 1 hour if not provided
      this.tokenExpiration = new Date(new Date().getTime() + expiresIn * 1000);
    }
    return this.token;
  }

  async updateJobState(jobId, state) {
    try {
      const token = await this.getToken();
      await axios.put(`${this.metricsEndpoint}/${jobId}/status`, null, {
        params: { state },
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
    } catch (error) {
      console.error(
        `Failed to update job: ${jobId} state to '${state}'`,
        error
      );
    }
  }

  async sendMetricsToEndpoint(jobId, metrics) {
    const common = {
      station_id: config.stationId,
      timestamp: metrics.timestamp,
    };

    const memMetricPayload = {
      ...common,
      metric_type: 'memory',
      value: metrics.memory.percentage,
    };

    const cpuMetricPayload = {
      ...common,
      metric_type: 'cpu',
      value: metrics.cpu.usage,
    };

    const netMetricPayload = {
      ...common,
      metric_type: 'network',
      rx_bytes: metrics.network.rxBytes.toString(),
      tx_bytes: metrics.network.txBytes.toString(),
    };

    try {
      const apiUrl = `${this.metricsEndpoint}/${jobId}/metrics`;
      const token = await this.getToken();
      const headers = {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      };

      await Promise.all([
        axios.post(apiUrl, memMetricPayload, { headers }),
        axios.post(apiUrl, cpuMetricPayload, { headers }),
        axios.post(apiUrl, netMetricPayload, { headers }),
      ]);

      console.log(`[${metrics.timestamp}] Sent metrics for job: ${jobId}`);
    } catch (error) {
      console.error('Failed to send metrics:', error);
    }
  }

  stopMetricsCollection(container) {
    const intervalId = this.activeMonitors.get(container);
    if (intervalId) {
      clearInterval(intervalId);
      this.activeMonitors.delete(container);
      console.log(`Stopped metrics monitoring for container ${container}`);
    }
  }

  // Stop all active metrics monitors
  stopAllMetricsCollection() {
    console.log('Stopping metrics monitoring for all containers');
    for (const [_, intervalId] of this.activeMonitors.entries()) {
      clearInterval(intervalId);
    }
    this.activeMonitors.clear();
  }
}

const monitoringClient = new ContainerMetricsMonitor(
  `${config.monitoringApiUrl}/jobs`
);

module.exports = monitoringClient;
