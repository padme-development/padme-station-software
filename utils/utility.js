const axios = require("axios");

const asyncHandler = fn => (req, res, next) =>
  Promise
    .resolve(fn(req, res, next))
    .catch((err) => {
      try {
        res.render('error', { user: req.user, error_msg: stringifyErrorMsg(err) });
      } catch (error) {
        next(err)
      }
    });

// https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-an-array-of-objects
const groupBy = function (xs, key) {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

const combineURLs = (baseURL, relativeURL) => {
  return relativeURL
    ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '')
    : baseURL;
}

const stringifyErrorMsg = (err) => {
  let msg = err;

  console.log(typeof err);
  console.log(err);

  try {

    // type of object
    try {
      if (typeof err === 'object') {
        msg = err.toString();
        if (msg === "[object Object]") {
          msg = JSON.stringify(err)
        }
      }
    } catch (error) {
      msg = 'Internal Server Error'
      console.log(error);
    }

  }
  catch (error) {
    console.log(error);
  }
  finally {
    return msg;
  }
}

const getCSTargetURL = () => {
  let endPoint = '';
  //If there is an endpoint defined use that
  const envEndpoint = process.env.CENTRALSERVICE_ENDPOINT;
  if (envEndpoint != null && envEndpoint != undefined && envEndpoint.length > 0) {
    endPoint = `/${envEndpoint}`
  }
  return `https://${process.env.CENTRALSERVICE_ADDRESS}:${process.env.CENTRALSERVICE_PORT}${endPoint}`
}

async function getKeycloakAccessToken() {
  const data = new URLSearchParams({
    grant_type: "password",
    client_id: "central-service",
    username: process.env.HARBOR_USER,
    password: process.env.HARBOR_PASSWORD,
    scope: "openid profile email offline_access",
  });

  try {
    const accessTokenUrl = `https://${process.env.AUTH_SERVER_ADDRESS}/auth/realms/pht/protocol/openid-connect/token`
    const response = await axios.post(accessTokenUrl, data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    return response.data;
  } catch (err) {
    throw new Error(`Error fetching access token: ${err.response ? err.response.data : err.message}`);
  }
}


module.exports = {
  asyncHandler,
  groupBy,
  combineURLs,
  stringifyErrorMsg,
  getCSTargetURL,
  getKeycloakAccessToken
}