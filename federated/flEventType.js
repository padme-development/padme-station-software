module.exports = {
    //Changes in the CS should be reflected here
    EventType:
    {
        NEW_LEARNING_ROUND: "new_learning_round",
        ABORTED: "aborted", 
        FINISHED: "finished",
    }
}